<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class DefaultControllerTest.
 */
class DefaultControllerTest extends WebTestCase
{
    public function testReadAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('Sign in');
        $form = $button->form([
            'username' => 'admin',
            'password' => 'admin',
        ]);
        $client->submit($form);

        $client->request('GET', '/article/read');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('Sign in');
        $form = $button->form([
            'username' => 'admin',
            'password' => 'admin',
        ]);
        $client->submit($form);
        $crawler = $client->request('GET', '/article/create');
        $button = $crawler->selectButton('article[submit]');

        $form = $button->form([
            'article[name]' => 'test',
            'article[description]' => 'fake article description',
            'article[created_at][month]' => '8',
            'article[created_at][day]' => '23',
            'article[created_at][year]' => '2019',
        ]);
        $client->submit($form);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testUpdateAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('Sign in');
        $form = $button->form([
            'username' => 'admin',
            'password' => 'admin',
        ]);
        $client->submit($form);
        $crawler = $client->request('GET', '/article/update/1');
        $button = $crawler->selectButton('article[submit]');
        $form = $button->form([
            'article[name]' => 'test edited',
            'article[description]' => 'fake article description edited',
            'article[created_at][month]' => '8',
            'article[created_at][day]' => '23',
            'article[created_at][year]' => '2019',
        ]);
        $client->submit($form);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testDeleteAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('Sign in');
        $form = $button->form([
            'username' => 'admin',
            'password' => 'admin',
        ]);
        $client->submit($form);
        $client->request('GET', '/article/delete/1');

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testSendEmail()
    {
        $client = static::createClient();
        $client->request('GET', '/email');

        $this->assertEmailCount(1);
    }
}
