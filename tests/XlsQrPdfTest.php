<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class DefaultControllerTest.
 */
class XlsHtmlPdfTest extends WebTestCase
{
    public function testPdf()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('Sign in');
        $form = $button->form([
            'username' => 'admin',
            'password' => 'admin',
        ]);
        $client->submit($form);
        $client->request('GET', '/article/pdf');
        var_dump($client->getResponse()->getContent());

        $this->assertEquals($client->getResponse()->headers->get('content-type'), 'application/pdf');
    }

    public function testQr()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('Sign in');
        $form = $button->form([
            'username' => 'admin',
            'password' => 'admin',
        ]);
        $client->submit($form);

        $client->request('GET', '/article/qr');
        $this->assertEquals($client->getResponse()->headers->get('content-type'), 'image/png');
    }

    public function testXls()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('Sign in');
        $form = $button->form([
            'username' => 'admin',
            'password' => 'admin',
        ]);
        $client->submit($form);

        $client->request('GET', '/article/xls');

        $this->assertEquals($client->getResponse()->headers->get('content-type'), 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    }
}
