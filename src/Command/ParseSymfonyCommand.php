<?php

namespace App\Command;

use App\Entity\ClassSymfony;
use App\Entity\InterfaceSymfony;
use App\Entity\NamespaceSymfony;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class ParseSymfonyCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /** @var string */
    private const API_URL = 'http://api.andreybolonin.com/';

    /**
     * @var EntityManagerInterface
     */
    private $url = 'http://api.andreybolonin.com/';

    /**
     * ParseSymfonyCommand constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName('app:parsesymfony')
            ->setDescription('Parsbiing site api.symfony.com')
            ->setHelp('This command parses the site ...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $namespace = new NamespaceSymfony();
        $namespace->setName('Symfony');
        $namespace->setUrl('http://api.andreybolonin.com/Symfony.html');
        $this->em->persist($namespace);
        $this->recursion('http://api.andreybolonin.com/Symfony.html', $namespace);
    }

    public function recursion($url, $parent)
    {
        $contentClasses = file_get_contents($url);
        $crawlerClasses = new Crawler($contentClasses);
        $classes = $crawlerClasses->filter('div#page-content > div.container-fluid.underlined > div.row > div.col-md-6 > a');
        foreach ($classes as $class) {
            $urlClass = self::API_URL.str_replace('../', '', $class->getAttribute('href'));
            $newClass = new ClassSymfony();
            $newClass->setNamespaceSymfony($parent);
            $newClass->setName($class->textContent);
            $newClass->setUrl($urlClass);
            $this->em->persist($newClass);
        }

        $crawlerClasses = new Crawler($contentClasses);
        $interfaces = $crawlerClasses->filter('div#page-content > div.container-fluid.underlined > div.row > div.col-md-6 > em > a > abbr');
        var_dump(count($interfaces));
        foreach ($interfaces as $interface) {
            $urlInterface = self::API_URL.str_replace('\\', '/', $interface->getAttribute('title').'.html');
            $newInterface = new InterfaceSymfony();
            $newInterface->setNamespaceSymfony($parent);
            $newInterface->setName($interface->textContent);
            $newInterface->setUrl($urlInterface);
            $this->em->persist($newInterface);
        }

        $content = file_get_contents($url);
        $crawler = new Crawler($content);
        $namespaces = $crawler->filter('div#page-content > div.namespace-list > a');
        var_dump(count($namespaces));
        foreach ($namespaces as $domElement) {
            $namespaceSymfony = new NamespaceSymfony();
            $namespaceSymfony->setUrl('http://api.andreybolonin.com/'.$domElement->getAttribute('href'));
            $namespaceSymfony->setName($domElement->nodeValue);
            $namespaceSymfony->setParent($parent);
            $this->em->persist($namespaceSymfony);
            $this->recursion('http://api.andreybolonin.com/'.str_replace('../', '', $domElement->getAttribute('href')), $namespaceSymfony);
        }
        $this->em->flush();
    }
}
