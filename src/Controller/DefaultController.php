<?php

namespace App\Controller;

//неймспейсы украл из примера

use App\Entity\Article;
use App\Form\ArticleType;
use Doctrine\ORM\EntityManagerInterface;
use Endroid\QrCode\QrCode;
use Knp\Snappy\Pdf;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/article")
 */
class DefaultController extends AbstractController
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * DefaultController constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/create", name="article_create")
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('article_read');
        }

        return $this->render('create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/read", name = "article_read")
     *
     * @return Response
     */
    public function readAction()
    {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository(Article::class)->findAll();

        return $this->render('article/read.html.twig', ['articles' => $articles]);
    }

    /**
     * @Route("/update/{id}", name = "article_update")
     *
     * @return Response
     */
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($id);
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('article_read');
        }

        return$this->render('article/update.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/delete/{id}", name = "article_delete")
     *
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($id);
        $em->remove($article);
        $em->flush();

        return$this->redirectToRoute('article_read');
    }

    /**
     * @Route("/article/locale/{locale}", name="article_locale")
     */
    public function changeLocaleAction(Request $request, string $locale): Response
    {
        $request->getSession()->set('_locale', $locale);

        return $this->redirectToRoute('article_read');
    }

    /**
     * @Route("/", name="index_all")
     */
    public function indexAction()
    {
        $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', 'https://packagist.org/packages/list.json');
        // декодируем json строки в переменную $packages
        $packages = json_decode($response->getContent(), true);

        return $this->render('default/index.html.twig', ['packages' => $packages['packageNames']]);
    }

    /**
     * @Route("/search/{name}", name="name")
     */
    public function searchName($name)
    {
        $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', 'https://packagist.org/search.json?q='.$name);
        $packages = json_decode($response->getContent(), true);
        dump($packages);

        return $this->render('default/search.html.twig', ['packages' => $packages['results']]);
    }

    /**
     * @Route("/search/vendor/{name}", name="vendor")
     */
    public function searchVendor($name)
    {
        $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', 'https://packagist.org/packages/list.json?vendor='.$name);
        $packages = json_decode($response->getContent(), true);
        //dump($packages);

        return $this->render('default/index.html.twig', ['packages' => $packages['packageNames']]);
    }

    /**
     * @Route("/pdf", name="pdf_hello_world")
     */
    public function pdfGeneration(Pdf $generator)
    {
        $generator->generate('https://google.com', __DIR__.'/../../public/pdf/pdf123.pdf');
        //add wkhtmltopdf
        $response = new BinaryFileResponse(__DIR__.'/../../public/pdf/pdf123.pdf');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'pdf123.pdf');

        return $response;
    }

    //   /**
    //    * @Route("/html", name="html_hello_world")
    //    */
//    public function htmlGeneration(Pdf $generator)
//    {
//        $html = $this->render('article/read.html.twig');
    //     123  $generator->generateFromHtml($html, __DIR__.'/../public/html->pdf/pfd777.png');
//
    //      return $this->redirectToRoute('article_read');
    //}

    /**
     * @Route("/xls", name="xls_hello_world")
     */
    public function xlsHelloWorld()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Hello World !');

        $writer = new Xlsx($spreadsheet);
        $writer->save(__DIR__.'/../../public/xls/hello_world.xlsx');
        $response = new BinaryFileResponse(__DIR__.'/../../public/xls/hello_world.xlsx');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'hello_world.xlsx');

        return $response;
    }

    /**
     * @Route("/qr", name="qr_code").
     */
    public function genratorQRCode()
    {
        $qrCode = new QrCode('https://www.hltv.org/');

        //header('Content-Type: '.$qrCode->getContentType());
        $qrCode->writeFile(__DIR__.'/../../public/qr/qrcode55.png');

        //return new BinaryFileResponse(__DIR__.'/../../public/qr/qrcode.png');

        $response = new BinaryFileResponse(__DIR__.'/../../public/qr/qrcode55.png');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'qrcode55.png');

        return $response;
    }
}
